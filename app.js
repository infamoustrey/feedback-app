const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const cookieSession = require('cookie-session');
const passport = require('passport');
const keys = require('./keys')

require('./models/User');
require('./services/passport');

mongoose.connect( keys.mongoURI );

const app = express();

app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [ keys.cookieKey ],
    })
); 

app.use(passport.initialize());
app.use(passport.session());


app.use('/auth', require('./routes/auth'));
app.use('/api', require('./routes/api'));

app.use('/static', express.static( path.resolve(__dirname + '/client/build/static') ));

app.get('/*', (req,res)=> res.sendFile( path.resolve(__dirname + '/client/build/index.html') ));

app.listen(5000) 