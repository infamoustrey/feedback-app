import axios from 'axios';
import {FETCH_USER} from './types';

const fetchUser = () => {

    return function( dispatch ) {
        axios.get('/auth/current-user')
            .then( response => dispatch({ type: FETCH_USER, payload: response}) );
    }

}