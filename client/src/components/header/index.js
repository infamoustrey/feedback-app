import React, {Component} from 'react';

class Header extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <nav>
                <div class="nav-wrapper">
                    <a href="/" class="left brand-logo">Feedback App </a>
                    <ul id="nav-mobile" class="right hide-on-med-and-down">
                        <li>
                            <a href="/auth/google">
                               Login With Google
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        ) 
    }

}

export default Header;